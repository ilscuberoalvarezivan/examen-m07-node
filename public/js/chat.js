$(document).ready(function () {
  //Inicializa socket con IO
  const socket = io();

  var sala = $('#sala').val();

  if (sala != "all") {
    socket.emit("joinRoom", { room: sala });
  }

  //Cuando cambia el select redirigimos a la URL del chat
  $('#selectRoom').on("change", () => {
    var sala = $(this).find("option:selected").val();
    window.location.href = "/chat/" + sala;
  })

  //Accion cuando el usuario envia mensaje con submit
  $("#chat").submit(function (e) {
    e.preventDefault();
    var msg = $("#msg").val();
    var autor = $("#autor").val();
    $("#chatBox").append(`<p>${msg}<p>`);
    socket.emit("newMsg", { msg: msg, autor: autor, sala: sala });
  });

  //Acciones a realizar cuando se detecta actividad en el canal newMsg
  socket.on("newMsg", function (data) {
    msg = data["msg"];
    autor = data["autor"];
    $("#chatBox").append(`<p style="text-align:left">${autor}: ${msg}<p>`);
  })
});