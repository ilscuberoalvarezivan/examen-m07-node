var express = require("express"),
  path = require("path"),
  router = express.Router(),
  ctrlDir = "/app/app/controllers",
  carsCtrl = require(path.join(ctrlDir, "cars")),
  rentsCtrl = require(path.join(ctrlDir, "rents")),
  clientsCtrl = require(path.join(ctrlDir, "clients"));


router.route("/chat/:id").get(async function (req, res) {
  res.render("chat.pug", { sala: req.params.id });
});
router.route("/rent/new").get(async function (req, res) {
  var cars = await carsCtrl.getAll(req);
  var clients = await clientsCtrl.getAll(req);
  res.render("form.pug", { cars: cars, clients: clients });
});

router.route("/rent/new").post(async function (req, res) {
  var split = req.body.car.split("/");
  console.log(split);
  var car = await carsCtrl.find(split[split.length - 1]);
  req.body.price = Number(req.body.days) * car.price;
  req.body.days = undefined;
  await rentsCtrl.create(req);
  res.redirect("/rent/new");
});

router.route("/rent/list").get(async function (req, res) {
  var rents = await rentsCtrl.getAll();
  res.render("list.pug", { rents: rents });
});


module.exports = router;
