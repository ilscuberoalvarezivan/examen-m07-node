var path = require("path"),
  express = require("express"),
  router = express.Router(),
  ctrlDir = "/app/app/controllers",
  carsCtrl = require(path.join(ctrlDir, "cars")),
  rentsCtrl = require(path.join(ctrlDir, "rents")),
  clientsCtrl = require(path.join(ctrlDir, "clients"));

/* CARS API */
router.route("/cars/").get(async function (req, res) {
  var cars = await carsCtrl.getAll(req);
  res.status(200).jsonp(cars);
});
router.route("/cars/:id").get(async function (req, res) {
  var car = await carsCtrl.find(req.params.id);
  res.status(200).jsonp(car);
});
router.route("/cars/").post(async function (req, res) {
  carsCtrl.create(req.body);
  res.status(200);
});
router.route("/cars/:id").put(async function (req, res) {
  carsCtrl.edit(req);
  res.status(200);
});

router.route("/cars/:id").delete(async function (req, res) {
  carsCtrl.delete(req);
  res.status(200);
});

/* CLIENTS API */
router.route("/clients/").get(async function (req, res) {
  var cars = await clientsCtrl.getAll(req);
  res.status(200).jsonp(cars);
});
router.route("/clients/:id").get(async function (req, res) {
  var car = await clientsCtrl.find(req.params.id);
  res.status(200).jsonp(car);
});
router.route("/clients/").post(async function (req, res) {
  clientsCtrl.create(req.body);
  res.status(200);
});
router.route("/clients/:id").put(async function (req, res) {
  clientsCtrl.edit(req);
  res.status(200);
});

router.route("/clients/:id").delete(async function (req, res) {
  clientsCtrl.delete(req);
  res.status(200);
});

/* RENTS API */
router.route("/rents/").get(async function (req, res) {
  var cars = await rentsCtrl.getAll(req);
  res.status(200).jsonp(cars);
});
router.route("/rents/:id").get(async function (req, res) {
  var car = await rentsCtrl.find(req.params.id);
  res.status(200).jsonp(car);
});
router.route("/rents/").post(async function (req, res) {
  rentsCtrl.create(req.body);
  res.status(200);
});
router.route("/rents/:id").put(async function (req, res) {
  rentsCtrl.edit(req);
  res.status(200);
});
router.route("/rents/:id").delete(async function (req, res) {
  rentsCtrl.delete(req);
  res.status(200);
});
module.exports = router;
