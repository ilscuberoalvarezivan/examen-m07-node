var mongoose = require("mongoose"),
    Schema = mongoose.Schema

var car = new Schema({
    id: { type: String },
    brand: { type: String },
    model: { type: String },
    price: { type: Number },
});

module.exports = mongoose.model("car", car);