var mongoose = require("mongoose"),
    Schema = mongoose.Schema

var rent = new Schema({
    client: { type: String },
    car: { type: String },
    price: { type: Number },
});

module.exports = mongoose.model("rent", rent);