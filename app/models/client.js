var mongoose = require("mongoose"),
    Schema = mongoose.Schema

var client = new Schema({
    id: { type: String },
    name: { type: String },
    surname: { type: String },
});

module.exports = mongoose.model("client", client);