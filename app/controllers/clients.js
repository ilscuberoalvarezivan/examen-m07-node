var mongoose = require("mongoose"),
  Model = require("../models/client");

exports.getAll = async (req, res, next) => {
  var params = {};
  if (req != undefined && req.query != undefined) {
    params = req.query;
  }
  var clients = Model.find(params);
  return clients;
};

exports.find = async (req, res, next) => {
  var client = Model.findOne({ id: req });
  return client;
};
exports.create = async (req, res, next) => {
  var cliente = new Model(req.body);
  cliente.save((err, res) => {
    return res;
  });
};

exports.edit = async (req, res, next) => {
  Model.updateOne({ id: req.params.id }, req.body);
  return;
};

exports.delete = async (req, res, next) => {
  Model.deleteOne({ id: req.params.id });
  return;
};
