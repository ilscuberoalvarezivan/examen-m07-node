var mongoose = require("mongoose"),
  Model = require("../models/car");

exports.getAll = async (req, res, next) => {
  var params = {};
  if (req != undefined && req.query != undefined) {
    params = req.query;
  }
  var cars = Model.find(params);
  return cars;
};

exports.find = async (req, res, next) => {
  var car = Model.findOne({ id: req });
  return car;
};
exports.create = async (req, res, next) => {
  var coche = new Model(req.body);
  coche.save((err, res) => {
    return res;
  });
};

exports.edit = async (req, res, next) => {
  Model.updateOne({ id: req.params.id }, req.body);
  return
};

exports.delete = async (req, res, next) => {
  Model.deleteOne({ id: req.params.id });
  return
};
