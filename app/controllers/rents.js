var mongoose = require("mongoose"),
  Model = require("../models/rent");

exports.getAll = async (req, res, next) => {
  var params = {};
  if (req != undefined && req.query != undefined) {
    params = req.query;
  }
  var clients = Model.find(params);
  return clients;
};

exports.find = async (req, res, next) => {
  var client = Model.findOne({ _id: req });
  return client;
};

exports.edit = async (req, res, next) => {
  Model.updateOne({ _id: req.params.id }, req.body);
  return;
};

exports.delete = async (req, res, next) => {
  Model.deleteOne({ _id: req.params.id });
  return;
};

exports.create = async (req, res, next) => {
  console.log(req.body);
  var rent = new Model({ price: req.body.price, client: req.body.client, car: req.body.car });
  rent.save((err, res) => {
    return res;
  });
  return;
};